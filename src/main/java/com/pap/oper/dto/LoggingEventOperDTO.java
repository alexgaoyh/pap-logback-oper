package com.pap.oper.dto;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import com.pap.base.mybatis.plugin.annotation.MyBatisTableAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "操作日志")
public class LoggingEventOperDTO extends PapBaseEntity implements Serializable {
    /**
     *  ,所属表字段为logging_event.event_id
     */
    @MyBatisColumnAnnotation(name = "event_id", value = "logging_event_event_id", chineseNote = "", tableAlias = "logging_event")
    @ApiModelProperty(value = "")
    private Long eventId;

    /**
     *  ,所属表字段为logging_event.timestmp
     */
    private String timestmp;

    /**
     *  ,所属表字段为logging_event.logger_name
     */
    @MyBatisColumnAnnotation(name = "logger_name", value = "logging_event_logger_name", chineseNote = "", tableAlias = "logging_event")
    @ApiModelProperty(value = "")
    private String loggerName;

    /**
     *  ,所属表字段为logging_event.level_string
     */
    @MyBatisColumnAnnotation(name = "level_string", value = "logging_event_level_string", chineseNote = "", tableAlias = "logging_event")
    @ApiModelProperty(value = "")
    private String levelString;

    /**
     *  ,所属表字段为logging_event.thread_name
     */
    @MyBatisColumnAnnotation(name = "thread_name", value = "logging_event_thread_name", chineseNote = "", tableAlias = "logging_event")
    @ApiModelProperty(value = "")
    private String threadName;

    /**
     *  ,所属表字段为logging_event.reference_flag
     */
    @MyBatisColumnAnnotation(name = "reference_flag", value = "logging_event_reference_flag", chineseNote = "", tableAlias = "logging_event")
    @ApiModelProperty(value = "")
    private Short referenceFlag;

    /**
     *  ,所属表字段为logging_event.caller_filename
     */
    @MyBatisColumnAnnotation(name = "caller_filename", value = "logging_event_caller_filename", chineseNote = "", tableAlias = "logging_event")
    @ApiModelProperty(value = "")
    private String callerFilename;

    /**
     *  ,所属表字段为logging_event.caller_class
     */
    @MyBatisColumnAnnotation(name = "caller_class", value = "logging_event_caller_class", chineseNote = "", tableAlias = "logging_event")
    @ApiModelProperty(value = "")
    private String callerClass;

    /**
     *  ,所属表字段为logging_event.caller_method
     */
    @MyBatisColumnAnnotation(name = "caller_method", value = "logging_event_caller_method", chineseNote = "", tableAlias = "logging_event")
    @ApiModelProperty(value = "")
    private String callerMethod;

    /**
     *  ,所属表字段为logging_event.caller_line
     */
    @MyBatisColumnAnnotation(name = "caller_line", value = "logging_event_caller_line", chineseNote = "", tableAlias = "logging_event")
    @ApiModelProperty(value = "")
    private String callerLine;

    /**
     *  ,所属表字段为logging_event.arg0
     */
    private String arg0;

    /**
     *  ,所属表字段为logging_event.arg1
     */
    private String arg1;

    /**
     *  ,所属表字段为logging_event.arg2
     */
    private String arg2;

    /**
     *  ,所属表字段为logging_event.arg3
     */
    private String arg3;

    /**
     *  ,所属表字段为logging_event.arg4
     */
    private String arg4;

    /**
     *  ,所属表字段为logging_event.arg5
     */
    private String arg5;

    /**
     *  ,所属表字段为logging_event.arg6
     */
    private String arg6;

    /**
     *  ,所属表字段为logging_event.arg7
     */
    private String arg7;

    /**
     *  ,所属表字段为logging_event.arg8
     */
    private String arg8;

    /**
     *  ,所属表字段为logging_event.arg9
     */
    private String arg9;

    /**
     *  ,所属表字段为logging_event.arg10
     */
    private String arg10;

    /**
     *  ,所属表字段为logging_event.arg11
     */
    private String arg11;

    /**
     *  ,所属表字段为logging_event.arg12
     */
    private String arg12;

    /**
     *  ,所属表字段为logging_event.arg13
     */
    private String arg13;

    /**
     *  ,所属表字段为logging_event.arg14
     */
    private String arg14;

    /**
     *  ,所属表字段为logging_event.arg15
     */
    private String arg15;

    /**
     *  ,所属表字段为logging_event.arg16
     */
    private String arg16;

    /**
     *  ,所属表字段为logging_event.arg17
     */
    private String arg17;

    /**
     *  ,所属表字段为logging_event.arg18
     */
    private String arg18;

    /**
     *  ,所属表字段为logging_event.arg19
     */
    private String arg19;

    /**
     *  ,所属表字段为logging_event.arg20
     */
    private String arg20;

    /**
     *  ,所属表字段为logging_event.arg21
     */
    private String arg21;

    /**
     *  ,所属表字段为logging_event.arg22
     */
    private String arg22;

    /**
     *  ,所属表字段为logging_event.arg23
     */
    private String arg23;

    /**
     *  ,所属表字段为logging_event.arg24
     */
    private String arg24;

    /**
     *  ,所属表字段为logging_event.arg25
     */
    private String arg25;

    /**
     *  ,所属表字段为logging_event.arg26
     */
    private String arg26;

    /**
     *  ,所属表字段为logging_event.arg27
     */
    private String arg27;

    /**
     *  ,所属表字段为logging_event.arg28
     */
    private String arg28;

    /**
     *  ,所属表字段为logging_event.arg29
     */
    private String arg29;

    /**
     *  ,所属表字段为logging_event.arg30
     */
    private String arg30;

    /**
     *  ,所属表字段为logging_event.arg31
     */
    private String arg31;

    /**
     *  ,所属表字段为logging_event.formatted_message
     */
    @MyBatisColumnAnnotation(name = "formatted_message", value = "logging_event_formatted_message", chineseNote = "", tableAlias = "logging_event")
    @ApiModelProperty(value = "")
    private String formattedMessage;

    private static final long serialVersionUID = 1L;

    @Override
    public String getDynamicTableName() {
        return "logging_event";
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public String getTimestmp() {
        return timestmp;
    }

    public void setTimestmp(String timestmp) {
        this.timestmp = timestmp;
    }

    public String getLoggerName() {
        return loggerName;
    }

    public void setLoggerName(String loggerName) {
        this.loggerName = loggerName;
    }

    public String getLevelString() {
        return levelString;
    }

    public void setLevelString(String levelString) {
        this.levelString = levelString;
    }

    public String getThreadName() {
        return threadName;
    }

    public void setThreadName(String threadName) {
        this.threadName = threadName;
    }

    public Short getReferenceFlag() {
        return referenceFlag;
    }

    public void setReferenceFlag(Short referenceFlag) {
        this.referenceFlag = referenceFlag;
    }

    public String getCallerFilename() {
        return callerFilename;
    }

    public void setCallerFilename(String callerFilename) {
        this.callerFilename = callerFilename;
    }

    public String getCallerClass() {
        return callerClass;
    }

    public void setCallerClass(String callerClass) {
        this.callerClass = callerClass;
    }

    public String getCallerMethod() {
        return callerMethod;
    }

    public void setCallerMethod(String callerMethod) {
        this.callerMethod = callerMethod;
    }

    public String getCallerLine() {
        return callerLine;
    }

    public void setCallerLine(String callerLine) {
        this.callerLine = callerLine;
    }

    public String getArg0() {
        return arg0;
    }

    public void setArg0(String arg0) {
        this.arg0 = arg0;
    }

    public String getArg1() {
        return arg1;
    }

    public void setArg1(String arg1) {
        this.arg1 = arg1;
    }

    public String getArg2() {
        return arg2;
    }

    public void setArg2(String arg2) {
        this.arg2 = arg2;
    }

    public String getArg3() {
        return arg3;
    }

    public void setArg3(String arg3) {
        this.arg3 = arg3;
    }

    public String getArg4() {
        return arg4;
    }

    public void setArg4(String arg4) {
        this.arg4 = arg4;
    }

    public String getArg5() {
        return arg5;
    }

    public void setArg5(String arg5) {
        this.arg5 = arg5;
    }

    public String getArg6() {
        return arg6;
    }

    public void setArg6(String arg6) {
        this.arg6 = arg6;
    }

    public String getArg7() {
        return arg7;
    }

    public void setArg7(String arg7) {
        this.arg7 = arg7;
    }

    public String getArg8() {
        return arg8;
    }

    public void setArg8(String arg8) {
        this.arg8 = arg8;
    }

    public String getArg9() {
        return arg9;
    }

    public void setArg9(String arg9) {
        this.arg9 = arg9;
    }

    public String getArg10() {
        return arg10;
    }

    public void setArg10(String arg10) {
        this.arg10 = arg10;
    }

    public String getArg11() {
        return arg11;
    }

    public void setArg11(String arg11) {
        this.arg11 = arg11;
    }

    public String getArg12() {
        return arg12;
    }

    public void setArg12(String arg12) {
        this.arg12 = arg12;
    }

    public String getArg13() {
        return arg13;
    }

    public void setArg13(String arg13) {
        this.arg13 = arg13;
    }

    public String getArg14() {
        return arg14;
    }

    public void setArg14(String arg14) {
        this.arg14 = arg14;
    }

    public String getArg15() {
        return arg15;
    }

    public void setArg15(String arg15) {
        this.arg15 = arg15;
    }

    public String getArg16() {
        return arg16;
    }

    public void setArg16(String arg16) {
        this.arg16 = arg16;
    }

    public String getArg17() {
        return arg17;
    }

    public void setArg17(String arg17) {
        this.arg17 = arg17;
    }

    public String getArg18() {
        return arg18;
    }

    public void setArg18(String arg18) {
        this.arg18 = arg18;
    }

    public String getArg19() {
        return arg19;
    }

    public void setArg19(String arg19) {
        this.arg19 = arg19;
    }

    public String getArg20() {
        return arg20;
    }

    public void setArg20(String arg20) {
        this.arg20 = arg20;
    }

    public String getArg21() {
        return arg21;
    }

    public void setArg21(String arg21) {
        this.arg21 = arg21;
    }

    public String getArg22() {
        return arg22;
    }

    public void setArg22(String arg22) {
        this.arg22 = arg22;
    }

    public String getArg23() {
        return arg23;
    }

    public void setArg23(String arg23) {
        this.arg23 = arg23;
    }

    public String getArg24() {
        return arg24;
    }

    public void setArg24(String arg24) {
        this.arg24 = arg24;
    }

    public String getArg25() {
        return arg25;
    }

    public void setArg25(String arg25) {
        this.arg25 = arg25;
    }

    public String getArg26() {
        return arg26;
    }

    public void setArg26(String arg26) {
        this.arg26 = arg26;
    }

    public String getArg27() {
        return arg27;
    }

    public void setArg27(String arg27) {
        this.arg27 = arg27;
    }

    public String getArg28() {
        return arg28;
    }

    public void setArg28(String arg28) {
        this.arg28 = arg28;
    }

    public String getArg29() {
        return arg29;
    }

    public void setArg29(String arg29) {
        this.arg29 = arg29;
    }

    public String getArg30() {
        return arg30;
    }

    public void setArg30(String arg30) {
        this.arg30 = arg30;
    }

    public String getArg31() {
        return arg31;
    }

    public void setArg31(String arg31) {
        this.arg31 = arg31;
    }

    public String getFormattedMessage() {
        return formattedMessage;
    }

    public void setFormattedMessage(String formattedMessage) {
        this.formattedMessage = formattedMessage;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", eventId=").append(eventId);
        sb.append(", timestmp=").append(timestmp);
        sb.append(", loggerName=").append(loggerName);
        sb.append(", levelString=").append(levelString);
        sb.append(", threadName=").append(threadName);
        sb.append(", referenceFlag=").append(referenceFlag);
        sb.append(", callerFilename=").append(callerFilename);
        sb.append(", callerClass=").append(callerClass);
        sb.append(", callerMethod=").append(callerMethod);
        sb.append(", callerLine=").append(callerLine);
        sb.append(", arg0=").append(arg0);
        sb.append(", arg1=").append(arg1);
        sb.append(", arg2=").append(arg2);
        sb.append(", arg3=").append(arg3);
        sb.append(", arg4=").append(arg4);
        sb.append(", arg5=").append(arg5);
        sb.append(", arg6=").append(arg6);
        sb.append(", arg7=").append(arg7);
        sb.append(", arg8=").append(arg8);
        sb.append(", arg9=").append(arg9);
        sb.append(", arg10=").append(arg10);
        sb.append(", arg11=").append(arg11);
        sb.append(", arg12=").append(arg12);
        sb.append(", arg13=").append(arg13);
        sb.append(", arg14=").append(arg14);
        sb.append(", arg15=").append(arg15);
        sb.append(", arg16=").append(arg16);
        sb.append(", arg17=").append(arg17);
        sb.append(", arg18=").append(arg18);
        sb.append(", arg19=").append(arg19);
        sb.append(", arg20=").append(arg20);
        sb.append(", arg21=").append(arg21);
        sb.append(", arg22=").append(arg22);
        sb.append(", arg23=").append(arg23);
        sb.append(", arg24=").append(arg24);
        sb.append(", arg25=").append(arg25);
        sb.append(", arg26=").append(arg26);
        sb.append(", arg27=").append(arg27);
        sb.append(", arg28=").append(arg28);
        sb.append(", arg29=").append(arg29);
        sb.append(", arg30=").append(arg30);
        sb.append(", arg31=").append(arg31);
        sb.append(", formattedMessage=").append(formattedMessage);
        sb.append("]");
        return sb.toString();
    }
}