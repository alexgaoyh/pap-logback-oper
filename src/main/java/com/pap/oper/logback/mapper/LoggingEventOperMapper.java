package com.pap.oper.logback.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.oper.logback.entity.LoggingEventOper;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface LoggingEventOperMapper extends PapBaseMapper<LoggingEventOper> {
    int deleteByPrimaryKey(Long eventId);

    int selectCountByMap(Map<Object, Object> map);

    List<LoggingEventOper> selectListByMap(Map<Object, Object> map);

    LoggingEventOper selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(LoggingEventOper record);

    int insertSelective(LoggingEventOper record);

    LoggingEventOper selectByPrimaryKey(Long eventId);

    int updateByPrimaryKeySelective(LoggingEventOper record);

    int updateByPrimaryKeyWithBLOBs(LoggingEventOper record);

    int updateByPrimaryKey(LoggingEventOper record);
}