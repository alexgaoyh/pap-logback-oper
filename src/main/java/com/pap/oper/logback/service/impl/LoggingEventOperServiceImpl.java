package com.pap.oper.logback.service.impl;

import com.pap.oper.logback.mapper.LoggingEventOperMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pap.oper.logback.entity.LoggingEventOper;
import com.pap.oper.logback.service.ILoggingEventOperService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Transactional
@Service("loggingEventOperService")
public class LoggingEventOperServiceImpl implements ILoggingEventOperService {

    @Resource(name = "loggingEventOperMapper")
    private LoggingEventOperMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<LoggingEventOper> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(LoggingEventOper record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(LoggingEventOper record) {
       return mapper.insertSelective(record);
    }

    @Override
    public LoggingEventOper selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(LoggingEventOper record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(LoggingEventOper record) {
      return mapper.updateByPrimaryKey(record);
    }
}