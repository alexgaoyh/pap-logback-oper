package com.pap.oper.logback.service;

import com.pap.oper.logback.entity.LoggingEventOper;

import java.util.List;
import java.util.Map;

public interface ILoggingEventOperService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<LoggingEventOper> selectListByMap(Map<Object, Object> map);

    int insert(LoggingEventOper record);

    int insertSelective(LoggingEventOper record);

    LoggingEventOper selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(LoggingEventOper record);

    int updateByPrimaryKey(LoggingEventOper record);
}
