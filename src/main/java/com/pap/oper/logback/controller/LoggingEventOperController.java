package com.pap.oper.logback.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.oper.dbs.TargetDataSource;
import com.pap.oper.dto.LoggingEventOperDTO;
import com.pap.oper.logback.entity.LoggingEventOper;
import com.pap.oper.logback.service.ILoggingEventOperService;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;


@RestController
@RequestMapping("/loggingeventoper")
@Api(value = "操作日志", tags = "操作日志", description="操作日志")
public class LoggingEventOperController {

	private static Logger logger  =  LoggerFactory.getLogger(LoggingEventOperController.class);
	
	@Resource(name = "loggingEventOperService")
	private ILoggingEventOperService loggingEventOperService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "loggingEventOperDTO", value = "应用查询参数", required = false, dataType = "LoggingEventOperDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@TargetDataSource("master")
	@PostMapping(value = "/master/query")
	public ResponseVO<LoggingEventOperDTO> masterQuery(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody LoggingEventOperDTO loggingEventOperDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
												@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(loggingEventOperDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<LoggingEventOper> list = loggingEventOperService.selectListByMap(map);

		//
		List<LoggingEventOperDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = loggingEventOperService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "loggingEventOperDTO", value = "应用查询参数", required = false, dataType = "LoggingEventOperDTO", paramType="body"),
			@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
			@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
			@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
			@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@TargetDataSource("slave")
	@PostMapping(value = "/slave/query")
	public ResponseVO<LoggingEventOperDTO> slaveQuery(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													   @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													   @RequestBody LoggingEventOperDTO loggingEventOperDTO,
													   @RequestParam Integer pageNo, @RequestParam Integer pageSize,
													   @RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
													   @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(loggingEventOperDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<LoggingEventOper> list = loggingEventOperService.selectListByMap(map);

		//
		List<LoggingEventOperDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = loggingEventOperService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}
	}

	private List<LoggingEventOperDTO> toDTO(List<LoggingEventOper> databaseList) {
		List<LoggingEventOperDTO> returnList = new ArrayList<LoggingEventOperDTO>();
		if(databaseList != null) {
			for(LoggingEventOper dbEntity : databaseList) {
				LoggingEventOperDTO dtoTemp = new LoggingEventOperDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<LoggingEventOper> toDB(List<LoggingEventOperDTO> dtoList) {
		List<LoggingEventOper> returnList = new ArrayList<LoggingEventOper>();
		if(dtoList != null) {
			for(LoggingEventOperDTO dtoEntity : dtoList) {
				LoggingEventOper dbTemp = new LoggingEventOper();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
