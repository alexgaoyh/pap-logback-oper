package com.pap.oper.dbs;

import java.lang.annotation.*;

/**
 * @Auther: alexgaoyh
 * @Date: 2019/3/4 17:28
 * @Description:
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TargetDataSource {
    String value();
}
